AJS.$(function () {

    function handleAction($projectHref) {
        var href = $projectHref.attr("href")
        var slashIndex = href.lastIndexOf("/")
        if (slashIndex == -1) {
            return;
        }
        var prjKey = href.substr(slashIndex + 1)
        AJS.$.ajax({
            url:contextPath + "/rest/api/2/project/" + prjKey,
            success:function (projectJson) {
                var mydialog = new JIRA.FormDialog({
                    content:function (ready) {
                        ready(JIRA.Templates.Demo.projectDetails({
                            name:projectJson.name,
                            description:projectJson.description,
                            lead:projectJson.lead.displayName
                        }));
                    }
                });
                mydialog.show()
            }
        })

    }
    AJS.$("#project-avatar").click(function () {
        $projectHref = AJS.$("#project-name-val")
        handleAction($projectHref)
    })

    AJS.$(".project > a").hover(function () {
        $projectHref = AJS.$(this);
        handleAction($projectHref)
    }, null)
});

//    var helloText = AJS.I18n.getText("hello.world") + " " + AJS.I18n.getText("from.atlascamp", 2012)
