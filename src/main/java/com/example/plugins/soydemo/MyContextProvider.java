package com.example.plugins.soydemo;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.template.soy.SoyTemplateRendererProvider;
import com.atlassian.jira.util.JiraVelocityUtils;
import com.atlassian.jira.util.collect.MapBuilder;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;
import com.google.common.collect.Maps;

import java.util.HashMap;
import java.util.Map;

public class MyContextProvider implements ContextProvider {

    private final JiraAuthenticationContext jiraAuthenticationContext;
    

    public MyContextProvider(JiraAuthenticationContext jiraAuthenticationContext) {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    @Override
    public void init(Map<String, String> params) throws PluginParseException {
    }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> context) {
        final HashMap<String, Object> res
                = Maps.newHashMap(JiraVelocityUtils.getDefaultVelocityParams(context, jiraAuthenticationContext));
        final Issue issue = (Issue) context.get("issue");
        final Project project = issue.getProjectObject();
        res.put("projectDetails", MapBuilder.newBuilder("name", project.getName(),
                "description", project.getDescription(),
                "lead", project.getLead() != null ? project.getLead().getDisplayName() : "None")
                .toHashMap());
        return res;
    }

}
